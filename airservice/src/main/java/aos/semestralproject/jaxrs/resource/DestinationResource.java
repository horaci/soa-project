package aos.semestralproject.jaxrs.resource;

import aos.semestralproject.service.FactoryService;
import aos.semestralproject.service.GeoLocalizationService;
import aos.semestralproject.model.Destination;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

/*
Class responsable to manage all the information about Destination Resource. It basiclly manages CRUD operations.
It also consumes GeoCode API.
*/

@Path("/destination")
public class DestinationResource {
    private static long lastUsedID = 0;
    private static HashMap<Long, Destination> destinations = new HashMap<Long, Destination>();

    @GET
    @Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
   public   List<Destination> getDestinations(){
        List<Destination> destinationsResults = new ArrayList<>();
        Iterator<Long> keySetIterator = destinations.keySet().iterator();
        while(keySetIterator.hasNext()){ 
            Long key = keySetIterator.next(); 
            destinationsResults.add(destinations.get(key));      
        }

        return destinationsResults;
    }
    
    
    @GET
    @Path("/{idInPath}")
    @Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})  //add MediaType.APPLICATION_XML if you want XML as well (don't forget @XmlRootElement)
    public Destination getDestination(@PathParam("idInPath") String idInPath){

        return destinations.get(Long.parseLong(idInPath));
    }
    
    
    @POST
    @Consumes({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})  //add MediaType.APPLICATION_XML if you want XML as well (don't forget @XmlRootElement)
    public String createNewDestination(Destination d,
            @Context UriInfo uriInfo) {
        String url = uriInfo.getAbsolutePath().toString();
        String nameDestination = d.getName();
        String text = "";
        GeoLocalizationService geoLocalitzacionService = FactoryService.getGeoLocalitzacionService();
        String[] localization = geoLocalitzacionService.getLocalization(d.getName());
        String lat = localization[0];
        String lng = localization[1];
            d.setId(lastUsedID++);
            d.setUrl(url+"/"+d.getId());
            d.setLat(Float.parseFloat(lat));
            d.setLon(Float.parseFloat(lng));
            destinations.put(d.getId(), d);      
            text += d.getName() + " lat " + lat + " lng " + lng+ " accepted POST ";
        return text;
    }
    
    @PUT
    @Consumes({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})  //add MediaType.APPLICATION_XML if you want XML as well (don't forget @XmlRootElement)
    @Path("/{idInPath}")
    public String updateDestination(@PathParam("idInPath") String idInPath, Destination d){
        long idD = Long.parseLong(idInPath);
        if(destinations.containsKey(idD)){
            d.setId(idD); //this is not neccessary
            destinations.put(idD, d);
            return "accepted for PUT "+idInPath;   
        } else {
            return "Destination with id:"+idD+" does not exist";
        }
    }
    
    @DELETE
    @Path("/{idInPath}")
    public String deleteDestination(@PathParam("idInPath") String idInPath){

        Object value = destinations.remove(Long.parseLong(idInPath));
        if (value ==null) return "It does not exist any destination with this Identifier ";
        return "Following value is removed from Map: " +value;
    }
    
    //return Destination d with d.id = id
    public static Destination getDestination(long id){
        return destinations.get(id);
    }
}