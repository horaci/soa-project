package aos.semestralproject.jaxrs.resource;

import aos.semestralproject.service.FactoryService;
import aos.semestralproject.service.CitiesDistanceService;
import aos.semestralproject.model.Flight;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

/*
Class responsable to manage all the information about Flight Resource. It manages CRUD operations and control
how many free seats are. It also consumes Rome2Rio API.
*/

@Path("/flight")
public class FlightResource {
    static long lastUsedID = 0;
    
    private static HashMap<Long, Flight> flights = new HashMap<Long, Flight>();
    private static HashMap<Long, Integer> seatsFree = new HashMap<Long, Integer>();

    @GET
    @Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
    public List<Flight> getFlights(@HeaderParam("X-order") String xOrder,
            @HeaderParam("X-Filter") String xFilter,
            @HeaderParam("X-Base") String xBase,
            @HeaderParam("X-Offset") String xOffset,    
            @Context HttpServletResponse response){
        int offset = getIntegerFromHeader(xOffset);
        int base = getIntegerFromHeader(xBase);
                //prepares dates from X-Filter
        Date minDate = null;
        Date maxDate = null;
        try{
            String xFilterMin = xFilter.substring("dateOfDepartureFrom=".length(), xFilter.indexOf(",dateOfDepartureTo="));
            String pattern = ",dateOfDepartureTo=";
            String xFilterMax = xFilter.substring(xFilter.lastIndexOf(pattern)+pattern.length());
            
            if(!xFilterMin.trim().isEmpty() && !xFilterMax.trim().isEmpty()){
                minDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX").parse(xFilterMin);
                maxDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX").parse(xFilterMax);
            }
        } catch(Exception e){
            System.err.println("error while parsing date"+e.getLocalizedMessage());
        }
        
        //selects filtered values or all values
        List<Flight> filteredFlights = new ArrayList<>();
        Iterator<Long> keySetIterator = flights.keySet().iterator();
        while(keySetIterator.hasNext()){ 
            //filter flights by date
            Long key = keySetIterator.next(); 
            Flight actualF = flights.get(key);
            if( minDate == null || maxDate == null){
                filteredFlights.add(actualF);      
            } else {
                if(actualF.getDateOfDeparture() != null && actualF.getDateOfDeparture().compareTo(minDate) >= 0
                        && actualF.getDateOfDeparture().compareTo(maxDate) <= 0){
                    filteredFlights.add(actualF);
                } 
            }
        }
        
        //order flights
        orderFlights(filteredFlights, xOrder);
        
        //pagination Offset and Base
        List<Flight> flightsresult = new ArrayList<>();
        if(offset < filteredFlights.size()){
            for(int i = offset;i<filteredFlights.size();i++){
                //check base, if base is set
                if(i >= offset+base && base > 0){
                    break;
                }
                flightsresult.add(filteredFlights.get(i));
            }
        }
        /* else return empty arraylist */
        
        //also set header of response X-Count-records: filteredFlights.size()
        response.setHeader("X-Count-records", ""+filteredFlights.size());
        return flightsresult;
    }
    
    /**
     * Orders given list of flights based on xOrder value.
     * if X-Order is just ":", or if no "asc/desc" is selected do nothing. 
     * Else ordering is done for dateOfDeparture, name or price. based on X-Order.
     * Example: X-Order = "name:asc"
     * @param flights list to sort
     * @param xOrder value from header X-Order
     */
    private void orderFlights(List<Flight> flights, String xOrder){
        try{
            String orderBy = xOrder.substring(0, xOrder.indexOf(":"));
            String order = xOrder.substring(xOrder.indexOf(":")+1);
            if(order.equals("asc") || order.equals("desc")){
                switch (orderBy) {
                    case "dateOfDeparture":
                        flights.sort(new Comparator<Flight>() {
                            @Override
                            public int compare(Flight o1, Flight o2) {
                                return o1.getDateOfDeparture().compareTo(o2.getDateOfDeparture());
                            }
                        }); break;
                    case "name":
                        flights.sort(new Comparator<Flight>() {
                            @Override
                            public int compare(Flight o1, Flight o2) {
                                return o1.getName().compareTo(o2.getName());
                            }
                        }); break;
                    case "price":
                        flights.sort(new Comparator<Flight>() {
                            @Override
                            public int compare(Flight o1, Flight o2) {
                                return (int) (o1.getPrice() - o2.getPrice());
                            }
                        }); break;
                }

                if(order.equals("desc")){
                    Collections.reverse(flights);
                }
            } 
        } catch(Exception e){
            //do nothing
            System.err.println("error in ordering"+e.getLocalizedMessage());
        }
    
    }
    
    
    /**
     * Parses input String into integer value. If it is not possible then 0 is returned.
     * Also if value < 0 then 0 is returned
     * @param headerValue
     * @return 
     */
    private int getIntegerFromHeader(String headerValue){
        int value;
        try{
            value = Integer.parseInt(headerValue);
            if(value < 0){
                value = 0;
            }
        } catch(NumberFormatException e){
            value = 0;
        }
        return value;
    }
    
    @GET
    @Path("/{idInPath}")
    @Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})  //add MediaType.APPLICATION_XML if you want XML as well (don't forget @XmlRootElement)
    public static Flight getFlight(@PathParam("idInPath") String idInPath){
        return flights.get(Long.parseLong(idInPath));
    }
    
    
    @POST
    @Consumes({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})  //add MediaType.APPLICATION_XML if you want XML as well (don't forget @XmlRootElement)
    public String createNewFlight(Flight f, @Context UriInfo uriInfo) throws MalformedURLException, IOException{
        
        String url = uriInfo.getAbsolutePath().toString();
        f.setId(lastUsedID++);
        f.setUrl(url+"/"+f.getId());
        flights.put(f.getId(), f);
        seatsFree.put(f.getId(), f.getSeats());
        //CODE FOR ROME2RIO
        String text = "";
        CitiesDistanceService citiesDistanceService = FactoryService.getCitiesDistanceService();
        String cityFrom = DestinationResource.getDestination(f.getFrom()).getName();
        String cityTo = DestinationResource.getDestination(f.getTo()).getName();
        String distance = citiesDistanceService.getDistance(cityFrom, cityTo);
        f.setDistance(Float.parseFloat(distance));
        return "accepted POST: LastUsedID" + lastUsedID + f.toString() + " " + f.getId() + " from " + 
                    DestinationResource.getDestination(f.getFrom()).getName() + " to " + DestinationResource.getDestination(f.getTo()).getName()+" distance="+text;
    }
    
    @PUT
    @Consumes({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})  //add MediaType.APPLICATION_XML if you want XML as well (don't forget @XmlRootElement)
    @Path("/{idInPath}")
    public String updateFlight(@PathParam("idInPath") String idInPath, Flight f){
        long idF = Long.parseLong(idInPath);
        
        //if flight exist
        if(flights.containsKey(idF)){
            f.setId(idF);
            flights.put(idF, f);
            return "accepted for PUT "+idInPath;   
        } else {
            return "Flight with id:"+idF+" does not exist";
        }
        
    }
    
    @DELETE
    @Path("/{idInPath}")
    public String deleteFlight(@PathParam("idInPath") String idInPath){
        Object value = flights.remove(Long.parseLong(idInPath));
        if (value ==null) return "It does not exist any flight with this Identifier ";
        return "Following value is removed from Map: " +value;
    }
    
    //Return number of free seats for flight f with f.id = id
    public static int getSeatsFree(long id) {
        return seatsFree.get(id);
    }
    
    //Update number of freeseats for fligh f with f.id = id.    
    public static void setSeatsFree(long id,int seatsReserved, int seatsWereReserved) {
        int seats=seatsFree.get(id);
        seats = seats-seatsReserved + seatsWereReserved;  
        seatsFree.put(id, seats);
    }     
}