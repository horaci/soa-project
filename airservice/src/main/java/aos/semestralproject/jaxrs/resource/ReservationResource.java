package aos.semestralproject.jaxrs.resource;

import aos.semestralproject.model.Reservation;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

/*
Class responsable to manage all the information about Reservation Resource. It basiclly manages CRUD operations.
*/


@Path("/reservation")
public class ReservationResource {
    static long lastUsedID = 0;
    final static String ADMIN_USER_LOGIN = "Boss";
    final static String ADMIN_USER_PW = "boss";
    
    public static HashMap<Long, Reservation> reservations = new HashMap<Long, Reservation>();
    @Context private SecurityContext securityContext;
    
    @GET
    @Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
    public List<Reservation> getReservations(){
        List<Reservation> reservationsResults = new ArrayList<>();
        Iterator<Long> keySetIterator = reservations.keySet().iterator();
        while(keySetIterator.hasNext()){ 
            Long key = keySetIterator.next(); 
            reservationsResults.add(reservations.get(key));      
        }
        return reservationsResults;
    }
    
    @GET
    @Path("/{idInPath}")
    @Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})  //add MediaType.APPLICATION_XML if you want XML as well (don't forget @XmlRootElement)
    public Response getReservation(@PathParam("idInPath") String idInPath,
            @HeaderParam("X-Password") String xPW,
            @HeaderParam("Authorization") String xAuth){
        
        Reservation selectedReservation = reservations.get(Long.parseLong(idInPath));
        if(selectedReservation.getPassword().equals(xPW)){

            return Response.status(Response.Status.OK).entity(selectedReservation.toString()).build();
            
        } else {
            //reservation PW is not right -> check Admin user
            if(xAuth != null){
                String[] user = getUserCredentials(xAuth);
                if(user[0].equals(ADMIN_USER_LOGIN) && user[1].equals(ADMIN_USER_PW)){
                    //admin logged - password is not neccessary
                    return Response.status(Response.Status.OK).entity(selectedReservation.toString()).build();
                    //TODO(just note for me) instead of this hardcoded username, I can send some HTTP request to general GET method with given headers
                    // and if it pass, then I know that the user has neccessary rights
                }
            }
            return Response.status(401).entity("Bad password or NOT logged Admin:").build();
            
        }
    }
    
    /**
     * Returns login and password from BASE Authentification string
     * @param authorization Basic Authorization header
     * @return array: String[0] - login, String[1] - password
     */
    private String[] getUserCredentials(String authorization){
        // Authorization: Basic base64credentials
        String base64Credentials = authorization.substring("Basic".length()).trim();
        String credentials = new String(Base64.getDecoder().decode(base64Credentials),
                Charset.forName("UTF-8"));
        // credentials = username:password
        return credentials.split(":",2);
        //http://stackoverflow.com/questions/16000517/how-to-get-password-from-http-basic-authentication
    }
    
    //it does not work for XML and I can not figure out why <- note from Mira to mira 
    @POST
    @Consumes({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})  //add MediaType.APPLICATION_XML if you want XML as well (don't forget @XmlRootElement)
    public String createNewReservation(Reservation r, @Context UriInfo uriInfo){
        FlightResource flightRes = new FlightResource();
        String text = "idFlight="+r.getIdflight();
        if (flightRes.getSeatsFree(r.getIdflight())-r.getSeats() >= 0) {
            flightRes.setSeatsFree(r.getIdflight(),r.getSeats(),0);
            String url = uriInfo.getAbsolutePath().toString();
            int newPassword = (int)(Math.random()*1000);    //not possible in production, but here it is ok
            r.setId(lastUsedID++);
            r.setPassword(""+newPassword);
            r.setUrl(url+"/"+r.getId());
            r.setState(aos.semestralproject.model.State.NEW);
            reservations.put(r.getId(), r);
            return "Reservation accepted id:"+r.getId()+" pw:"+r.getPassword()+" | "+text;
        }
        return "There are not seats available for this flight"+" | "+text;
    }
    
    @PUT
    @Consumes({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})  //add MediaType.APPLICATION_XML if you want XML as well (don't forget @XmlRootElement)
    @Path("/{idInPath}")
    public String updateReservation(@PathParam("idInPath") String idInPath, Reservation r, @HeaderParam("X-Password") String xPW){
        Reservation selectedReservation = reservations.get(Long.parseLong(idInPath));
        if(selectedReservation.getPassword().equals(xPW)){
            //TODO check seats-rules also here please
            FlightResource flightRes = new FlightResource();
            if ( ( flightRes.getSeatsFree(r.getIdflight()) + reservations.get(Long.parseLong(idInPath)).getSeats() ) -  r.getSeats() >= 0) {
                flightRes.setSeatsFree(r.getIdflight(),r.getSeats(),reservations.get(Long.parseLong(idInPath)).getSeats());
                long idD = Long.parseLong(idInPath);
                if(reservations.containsKey(idD)){
                    r.setId(idD);
                    reservations.put(idD, r);
                    return "accepted for PUT "+idInPath;   
                } else {
                    return "Reservation with id:"+idD+" does not exist";
                }
            }
            return "There are not seats available for this flight";
        }
        return "PW not match "+xPW;
    }
    
    @DELETE
    @Path("/{idInPath}")
    public String deleteReservation(@PathParam("idInPath") String idInPath){
        //reservations.remove(Long.parseLong(idInPath));
        Object value = reservations.remove(Long.parseLong(idInPath));
        if (value ==null) return "It does not exist any reservation with this Identifier ";
        return "Following value is removed from Map: " +value;
    }
}