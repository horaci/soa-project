/*
    Class that return information from GeoCodeAPI. Ir returns the latitude and longitude for nameDestination
    parameter.
 */
package aos.semestralproject.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import static javax.ws.rs.core.HttpHeaders.USER_AGENT;


public class GeoLocalizationService {
    //Must be encrypted in a deployment phase but for this project it is ok.
    String APIKey = "AIzaSyCv8ZTRCOEKiYj4GJwHwx8sqzVQY13CBrE";
    String urlAPI = "https://maps.googleapis.com/maps/api/geocode/json?address=";
    int startPosition = 7;
    int endPosition = 12;
    public String[] getLocalization(String nameDestination) {
        urlAPI = urlAPI+nameDestination+"&key="+APIKey;
        int indexOfLat=-1;
        int indexOfLng=-1;   
        StringBuffer response = new StringBuffer    ();
        String lat="";
        String lng="";
        try{
            URL obj = new URL(urlAPI);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            // optional default is GET
            con.setRequestMethod("GET");

            //add request header
            con.setRequestProperty("User-Agent", USER_AGENT);

            int responseCode = con.getResponseCode();
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;


            while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
            }
            in.close();
            indexOfLat = response.indexOf("lat");
            indexOfLng = response.indexOf("lng");
            
            lat = response.substring(indexOfLat+startPosition, indexOfLat+endPosition);
            lng = response.substring(indexOfLng+startPosition, indexOfLng+endPosition);
    
    } catch(Exception e){
        } 
        String[ ] latLng = new String[2];
        latLng[0] = lat;
        latLng[1] = lng;
        return latLng;
    }
}
