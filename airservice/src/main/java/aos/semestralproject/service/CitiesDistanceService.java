/*
    Class that return information from Rome2RioAPI. Ir returns the distance and duration (in plain) between
    cityFrom and cityTo parameters. 
 */
package aos.semestralproject.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import static javax.ws.rs.core.HttpHeaders.USER_AGENT;

public class  CitiesDistanceService {
     String urlAPI = "http://free.rome2rio.com/api/1.2/json/Search?key=vwiC3pvW&oName=";
     int distancePosition = 10;
     int durationPosition = 2;
     public String getDistance(String cityFrom,String cityTo ) throws MalformedURLException, IOException {   
            urlAPI = urlAPI+cityFrom+"&dName="+cityTo;

            URL obj = new URL(urlAPI);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            // optional default is GET
            con.setRequestMethod("GET");

            //add request header
            con.setRequestProperty("User-Agent", USER_AGENT);

            int responseCode = con.getResponseCode();
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();


            while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
            }
            in.close();
            System.out.println(response.toString());
            int indexOfDistance = response.indexOf("distance");
            int indexOfDuration = response.indexOf("duration");
            return  response.substring(indexOfDistance+distancePosition, indexOfDuration-durationPosition);
     }
}
