package aos.semestralproject.service;

/*
   Factory class that give an instance of a service. Now it just return one possible service for each functionality
    but in the future it could give more in case that one provider service is down.
 */

public class FactoryService { 
    public static  GeoLocalizationService getGeoLocalitzacionService() {
        return new GeoLocalizationService();
    }
    public static CitiesDistanceService getCitiesDistanceService() {
        return new CitiesDistanceService();
    }
}

