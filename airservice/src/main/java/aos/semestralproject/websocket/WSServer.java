/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package aos.semestralproject.websocket;


import java.io.IOException;
 
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

/** 
 * @ServerEndpoint gives the relative name for the end point
 * This will be accessed via ws://localhost:8080/"something"/websocket/connectionsCounter
 * Class counts number of parallely connected clients.
 */
@ServerEndpoint("/websocket/connectionsCounter") 
public class WSServer {

    private static final List<Session> activeSessions = Collections.synchronizedList(new ArrayList<Session>());

    private static final Logger LOGGER = 
            Logger.getLogger(WSServer.class.getName());
    
    /**
     * Adds new session into lists of sessions and sends actual number of 
     * clients to each client.
     * @param session connection session
     */
    @OnOpen
    public void onOpen(Session session) {
        LOGGER.log(Level.INFO, "New connection with client: {0}", 
                session.getId());
        
        activeSessions.add(session);
        shoutActualCount();
    }
    
    /**
     * Testing function which returns given message.
     * @param message
     * @param session client's session
     * @return info about given message 
     */
    @OnMessage
    public String onMessage(String message, Session session) {
        LOGGER.log(Level.INFO, "New message from Client [{0}]: {1}", 
                new Object[] {session.getId(), message});
        return "Server received [" + message + "] from id:"+session.getId()+" actual count="+activeSessions.size();
    }
    
    /**
     * Gets info about closed session, removes it from sessions list
     * and sends message about number of connected clients to each of them.
     * @param session client's session
     */
    @OnClose
    public void onClose(Session session) {
        LOGGER.log(Level.INFO, "Close connection for client: {0}", 
                session.getId());
        
        activeSessions.remove(session);
        shoutActualCount();
    }
    
    /**
     * Logs exception if occurs.
     * @param exception 
     * @param session client's session
     */
    @OnError
    public void onError(Throwable exception, Session session) {
        LOGGER.log(Level.INFO, "Error for client: {0}", session.getId());
    }

    /**
     * Go through all sessions and sends them a message about number of actual connections.
     * Uses static activeSessions list.
     */
    private void shoutActualCount(){
        synchronized (activeSessions) {
            int count = activeSessions.size();
            Iterator<Session> i = activeSessions.iterator(); // Must be in synchronized block
            while (i.hasNext()){
                sendActualCountToClient(i.next(), count);
            }
        }
    }

    /**
     * Sends given number to given session. Or logs IOException if it occurs.
     * @param client Sessiont of given client
     * @param count number of actual sessions, it will be sent
     */
    private void sendActualCountToClient(Session client, int count) {
        try {
            client.getBasicRemote().sendText(""+count);
        } catch (IOException ex) {
            Logger.getLogger(WSServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
