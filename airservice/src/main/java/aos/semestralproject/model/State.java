package aos.semestralproject.model;

import java.util.Arrays;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/*
Class represents a state concept in our model.
*/

@XmlType(name = "status")
@XmlEnum
public enum State {
    @XmlEnumValue(value = "new")
    NEW, 
    @XmlEnumValue(value = "paid")
    PAID, 
    @XmlEnumValue(value = "canceled")
    CANCELED
   
}
