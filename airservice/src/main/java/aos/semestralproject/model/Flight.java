package aos.semestralproject.model;

import java.util.Date;
import javax.xml.bind.annotation.XmlRootElement;

/*
Class represents a flight concept in our model.
*/


@XmlRootElement       //only needed if we also want to generate XML
public class Flight {
    private long id;
    private Date dateOfDeparture;
    private float distance;
    private float price;
    private int seats;
    private String name;       
    private long from;
    private long to;
    private String url;

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the dateOfDeparture
     */
    public Date getDateOfDeparture() {
        return dateOfDeparture;
    }

    /**
     * @param dateOfDeparture the dateOfDeparture to set
     */
    public void setDateOfDeparture(Date dateOfDeparture) {
        this.dateOfDeparture = dateOfDeparture;
    }

    /**
     * @return the distance
     */
    public float getDistance() {
        return distance;
    }

    /**
     * @param distance the distance to set
     */
    public void setDistance(float distance) {
        this.distance = distance;
    }

    /**
     * @return the price
     */
    public float getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(float price) {
        this.price = price;
    }

    /**
     * @return the seats
     */
    public int getSeats() {
        return seats;
    }

    /**
     * @param seats the seats to set
     */
    public void setSeats(int seats) {
        this.seats = seats;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the dfrom
     */
    public long getFrom() {
        return from;
    }

    /**
     * @param dfrom the dfrom to set
     */
    public void setFrom(long dfrom) {
        this.from = dfrom;
    }

    /**
     * @return the dto
     */
    public long getTo() {
        return to;
    }

    /**
     * @param dto the dto to set
     */
    public void setTo(long dto) {
        this.to = dto;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    
    
}