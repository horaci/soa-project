package aos.semestralproject.model;

import java.util.Date;
import javax.xml.bind.annotation.XmlRootElement;

/*
Class represents a reservation concept in our model.
*/

@XmlRootElement       //only needed if we also want to generate XML
public class Reservation {
    private long id;
    private int seats;
    private String password;
    private Date created;
    private State state;
    private long idflight;
    private String url;
    
    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the seats
     */
    public int getSeats() {
        return seats;
    }

    /**
     * @param seats the seats to set
     */
    public void setSeats(int seats) {
        this.seats = seats;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the created
     */
    public Date getCreated() {
        return created;
    }

    /**
     * @param created the created to set
     */
    public void setCreated(Date created) {
        this.created = created;
    }

    /**
     * @return the state
     */
    public State getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(State state) {
        this.state = state;
    }

    /**
     * @return the idflight
     */
    public long getIdflight() {
        return idflight;
    }

    /**
     * @param idflight the idflight to set
     */
    public void setIdflight(long idflight) {
        this.idflight = idflight;
    }
    
    public void cancel() {
        this.state = State.CANCELED;
    }
    

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "Reservation{" + "id=" + id + ", seats=" + seats + ", password=" + password + ", created=" + created + ", state=" + state + ", idflight=" + idflight + ", url=" + url + '}';
    }
    
    
}