/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aos.semestraproject.model;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * Defines states of reservation. NEW, PAID and CANCELED
 * @author avatar
 */
@XmlType(name = "status")
@XmlEnum
public enum State {
    @XmlEnumValue(value = "new")
    NEW, 
    @XmlEnumValue(value = "paid")
    PAID, 
    @XmlEnumValue(value = "canceled")
    CANCELED
   
}
