/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cz.cvut.fel.aos.printservice;

import aos.semestraproject.model.Reservation;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebService;

/**
 * Implementation of PrintService interface, which prints reservation into PDF file.
 * @author Mira
 */
@WebService(serviceName = "PrintService", 
            portName = "printservice", 
            endpointInterface = "cz.cvut.fel.aos.printservice.PrintService", 
            targetNamespace = "http://localhost:8080/cz.cvut.fel.aos.printservice")
public class PrintServiceImpl implements PrintService {

    /**
     * Tests method for WebService, it gets reservation and prints it
     * into PDF file.
     * @param res reservation which will be printed.
     * @return file.toString, just for test
     */
    @Override
    public String doFile(Reservation res) {
        PDFMaker pdf = new PDFMaker();
        String[] array = new String[2];
        array[0] = res.toString();
        array[1] = "ahoj";
        
        String response = "";
        try {
            File file = pdf.createPDFfile("name.pdf", array, ""+res.getId());
            response = file.toString(); //just for tests
        } catch (IOException ex) {
            response="IOException while creating PDF file";
            Logger.getLogger(PrintServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return response;
    }
    
}
