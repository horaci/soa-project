/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cz.cvut.fel.aos.printservice;

import aos.semestraproject.model.Reservation;
import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 * WebService interface with method for printing reservations.
 * @author Mira
 */
@WebService
public interface PrintService {
    
    /**
     * Prints reservation into file.
     * @param res reservation which wil be printed
     * @return file name
     */
    @WebMethod
    public String doFile(Reservation res);

    
}
