/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cz.cvut.fel.aos.printservice;

import java.io.File;
import java.io.IOException;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

/**
 * Class prepares method for pdf file creation.
 * @author Mira
 */
public class PDFMaker {
    
    /**
     * Creates new pdf file, add title with reservation ID and then prints each line 
     * of given array. Saves and returns this pdf file.
     * @param outputFileName name of output pdf file
     * @param linesOfText array of lines of text which will be printed into pdf
     * @param reservationID
     * @return PDF file with all the information
     * @throws IOException 
     */
    public File createPDFfile(String outputFileName, String[] linesOfText, String reservationID) throws IOException{
        //http://www.coderanch.com/how-to/java/PDFBoxExample
        
        File outputFile = new File("./"+outputFileName);
        // Create a document and add a page to it
        PDDocument document = new PDDocument();
        PDPage page1 = new PDPage(PDPage.PAGE_SIZE_A4);
        PDRectangle rect = page1.getMediaBox();
            // rect can be used to get the page width and height
        document.addPage(page1);

        // Create a new font object selecting one of the PDF base fonts
        PDFont fontBold = PDType1Font.HELVETICA_BOLD;
        PDFont fontMono = PDType1Font.COURIER;

        // Start a new content stream which will "hold" the to be created content
        PDPageContentStream cos = new PDPageContentStream(document, page1);

        int line = 0;

        // Define a text content stream using the selected font, move the cursor and draw some text
        cos.beginText();
        cos.setFont(fontBold, 14);
        cos.moveTextPositionByAmount(100, rect.getHeight() - 50*(++line));
        cos.drawString("Our Air Service Reservation, id:"+reservationID);
        cos.endText();

        int row=0;
        for(String rowText:linesOfText){
            cos.beginText();
            cos.setFont(fontMono, 10);
            cos.moveTextPositionByAmount(100, rect.getHeight() - 50*(line) - 20*(++row));
            cos.drawString(rowText);
            cos.endText();
        }
        
            // Make sure that the content stream is closed:
        cos.close();

        try {
            // Save the results and ensure that the document is properly closed:
            document.save(outputFile);
            System.out.println("saved");
        } catch (COSVisitorException ex) {
            System.out.println("EXCEPTION while saving");
        }
        document.close();
        return outputFile;
    }
}
